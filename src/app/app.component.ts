import { Component, VERSION } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public test = 'Ey mann!!';
  public changeClass = true;

  changeClassValue() {
    this.changeClass = !this.changeClass;
  }
}
